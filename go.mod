module github.com/hebcal/hebcal

go 1.13

require (
	github.com/hebcal/greg v1.0.0
	github.com/hebcal/hdate v1.0.1
	github.com/hebcal/hebcal-go v0.9.23
	github.com/pborman/getopt/v2 v2.1.0
)
